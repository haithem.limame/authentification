package com.esgi.model;

import com.esgi.annotation.Column;
import com.esgi.annotation.Entity;
import com.esgi.annotation.Id;

import java.util.Objects;
@Entity(name="Role")
public final class Role {
    @Id
    @Column(name="id")
   private Integer id;
    @Column(name="role")
   private String role;

    private Role(Integer id, String role) {
        this.id = Objects.requireNonNull(id);
        this.role = Objects.requireNonNull(role);
    }

    public Role() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role='" + role + '\'' +
                '}';
    }
}
