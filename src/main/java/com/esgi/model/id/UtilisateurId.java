package com.esgi.model.id;

import java.io.Serializable;
import java.util.Objects;

public class UtilisateurId implements Serializable {
    private final Integer value;

    public UtilisateurId(Integer value) {
        this.value = value;
    }
    public UtilisateurId of(Integer value) {
        return new UtilisateurId(value);
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UtilisateurId utilisateurId = (UtilisateurId) o;
        return value.equals(utilisateurId.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "UtilisateurId{" +
                "value=" + value +
                '}';
    }
}
