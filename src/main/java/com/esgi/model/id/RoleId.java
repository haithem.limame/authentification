package com.esgi.model.id;

import java.util.Objects;

public final class RoleId {
    private final Integer value;

    public RoleId(Integer value) {
        this.value = value;
    }
    public RoleId of(Integer value) {
        return new RoleId(value);
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleId roleId = (RoleId) o;
        return value.equals(roleId.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "RoleId{" +
                "value=" + value +
                '}';
    }
}
