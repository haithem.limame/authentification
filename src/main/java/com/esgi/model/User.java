package com.esgi.model;

import com.esgi.annotation.Column;
import com.esgi.annotation.Entity;
import com.esgi.annotation.Id;
import com.esgi.annotation.JoinColumn;

import java.util.Objects;

@Entity(name="User")
public class User {
    @Id
    @Column(name="id")
    private Integer id;
    @Column(name="lastname")
    private String lastname;
    @Column(name="firstname")
    private String firstname;
    @Column(name="email")
    private String email;
    @Column(name="password")
    private String password;
    @JoinColumn(name="role",referencedColumnName="role")
    private Role role;
    @Column(name="age")
    private int age;

    private User(Integer id, String lastname, String firstname, String email, String password, Role role, int age) {
        this.id = Objects.requireNonNull(id);
        this.lastname = Objects.requireNonNull(lastname);
        this.firstname = Objects.requireNonNull(firstname);
        this.email = Objects.requireNonNull(email);
        this.password = Objects.requireNonNull(password);
        this.role = Objects.requireNonNull(role);
        if (age < 1) {
            throw new IllegalStateException("You must provide a valid age value.");
        }
        this.age = age;

    }

    public User() {
    }

    public static User of(Integer id, String lastname, String firstname, String email, String password, Role role, int age) {
        return new User(id, lastname, firstname, email, password, role, age);
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }
}
