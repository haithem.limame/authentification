package com.esgi.orm;

import com.esgi.annotation.Column;
import com.esgi.annotation.Entity;
import com.esgi.annotation.Id;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class SqlRepository<T, ID> implements CrudRepository<T, ID> {
    private Connection connection = ConnectionManager.getConnection();

    private String getTableName() throws ClassNotFoundException {
        Type genericSuperclass = getGenericSuperclass();
        return Class.forName(genericSuperclass.getTypeName()).getAnnotation(Entity.class).name();

    }

    private String getIdName() throws ClassNotFoundException {
        Type genericSuperclass = getGenericSuperclass();
        return Arrays.stream(Class.forName(genericSuperclass.getTypeName()).getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Id.class))
                .map(field -> field.getAnnotation(Column.class).name())
                .collect(Collectors.toList())
                .get(0);

    }

    private List<String> getColumnsName() throws ClassNotFoundException {
        Type genericSuperclass = getGenericSuperclass();
        return Arrays.stream(Class.forName(genericSuperclass.getTypeName()).getDeclaredFields())
                .map(field -> field.getAnnotation(Column.class).name())
                .collect(Collectors.toList());
    }

    private List<Field> getFields() throws ClassNotFoundException {
        Type genericSuperclass = getGenericSuperclass();
        return Arrays.stream(Class.forName(genericSuperclass.getTypeName()).getDeclaredFields())
                .collect(Collectors.toList());
    }

    private Type getGenericSuperclass() throws ClassNotFoundException {
        return (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    @Override
    public Iterable<T> findAll() throws ClassNotFoundException, SQLException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        String tableName = getTableName();
        String SQL_SELECT = "SELECT * FROM " + tableName;
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Map<String, Object>> rows = new ArrayList<>();
        while (resultSet.next()) {
            Map<String, Object> map = new HashMap<>();
            for (Field field : getFields()) {
                String fieldName = field.getName();
                try {
                    map.put(fieldName, resultSet.getObject(fieldName));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            rows.add(map);
        }
        // map rows to class
        ObjectMapper<T> objectMapper = new ObjectMapper<>(Class.forName(getGenericSuperclass().getTypeName()));
        List<T> list = objectMapper.map(rows);
        return list;
    }
    @Override
    public Optional<T> findById(ID id) throws ClassNotFoundException, SQLException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        String tableName = getTableName();
        String idName = getIdName();
        String SQL_SELECT = "SELECT * FROM " + tableName + " WHERE " + idName + " = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT);
        preparedStatement.setInt(1, (Integer) id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Map<String, Object> element = new HashMap<>();
            for (Field field : getFields()) {
                String fieldName = field.getName();
                try {
                    element.put(fieldName, resultSet.getObject(fieldName));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // map rows to class
            ObjectMapper<T> objectMapper = new ObjectMapper<>(Class.forName(getGenericSuperclass().getTypeName()));
            return Optional.of(objectMapper.map(element));
        }
        return Optional.empty();
    }
    @Override
    public Boolean exists(ID id) throws ClassNotFoundException, SQLException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        String tableName = getTableName();
        String idName = getIdName();
        String SQL_EXIST = "SELECT 1 FROM " + tableName + " WHERE " + idName + " = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_EXIST);
        preparedStatement.setInt(1, (Integer) id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    @Override
    public void deleteById(ID id) throws ClassNotFoundException, SQLException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        String tableName = getTableName();
        String idName = getIdName();
        String SQL_DELETE = "DELETE FROM " + tableName + " WHERE " + idName + " = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE);
        preparedStatement.setInt(1, (Integer) id);
        int row = preparedStatement.executeUpdate();
    }
    @Override
    public void save(T entity) throws ClassNotFoundException, SQLException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, IntrospectionException {
        String tableName = getTableName();
        String idName = getIdName();
        List<String> columns = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        List<String> params = new ArrayList<>();
        List<Class<?>> types = new ArrayList<>();
        for (Field field : getFields()) {
            Object value = new PropertyDescriptor(field.getName(), Class.forName(getGenericSuperclass().getTypeName())).getReadMethod().invoke(entity);
            if (!field.getName().equals(idName)) {
                columns.add(field.getName());
                params.add("?");
                values.add(value);
                types.add(field.getType());
            }
        }
        String SQL_INSERT = "INSERT INTO " + tableName + " (" + String.join(",", columns) + ") VALUES (" + String.join(",", params) + ")";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
        for (int i = 0; i < values.size(); i++) {
                if (types.get(i).isAssignableFrom(String.class)) {
                    preparedStatement.setString(i + 1, values.get(i).toString());
                }else if (types.get(i).isAssignableFrom(Integer.class)) {
                    preparedStatement.setInt(i + 1, (Integer) values.get(i));
                }else if (types.get(i).isAssignableFrom(Boolean.class)) {
                    preparedStatement.setBoolean(i + 1, (Boolean) values.get(i));
                }else if (types.get(i).isAssignableFrom(Double.class)) {
                    preparedStatement.setDouble(i + 1, (Double) values.get(i));
                }
        }
        int row = preparedStatement.executeUpdate();
    }
    @Override
    public void update(T entity) throws ClassNotFoundException, SQLException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, IntrospectionException {
        String tableName = getTableName();
        String idName = getIdName();
        List<String> columns = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        List<Class<?>> types = new ArrayList<>();
        Object id = null;
        Class<?> idType = null;
        for (Field field : getFields()) {
            Object value = new PropertyDescriptor(field.getName(), Class.forName(getGenericSuperclass().getTypeName())).getReadMethod().invoke(entity);
            if (!field.getName().equals(idName)) {
                columns.add(field.getName());
                values.add(value);
                types.add(field.getType());
            }else {
                id = new PropertyDescriptor(field.getName(), Class.forName(getGenericSuperclass().getTypeName())).getReadMethod().invoke(entity);
                idType = field.getType();
            }
        }
        String SQL_UPDATE = "Update " + tableName + " SET " + (columns.size() > 1 ? String.join("= ? and ", columns) : columns.get(0) + " = ?") + " where "+idName+ " = ?" ;
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE);
        for (int i = 0; i < values.size(); i++) {
            if (types.get(i).isAssignableFrom(String.class)) {
                preparedStatement.setString(i + 1, values.get(i).toString());
            }else if (types.get(i).isAssignableFrom(Integer.class)) {
                preparedStatement.setInt(i + 1, (Integer) values.get(i));
            }else if (types.get(i).isAssignableFrom(Boolean.class)) {
                preparedStatement.setBoolean(i + 1, (Boolean) values.get(i));
            }else if (types.get(i).isAssignableFrom(Double.class)) {
                preparedStatement.setDouble(i + 1, (Double) values.get(i));
            }
        }
        if (idType.isAssignableFrom(String.class)) {
            preparedStatement.setString(getFields().size(),String.valueOf(id) );
        }else if (idType.isAssignableFrom(Integer.class)) {
            preparedStatement.setInt(getFields().size(), (Integer) id);
        }else if (idType.isAssignableFrom(Boolean.class)) {
            preparedStatement.setBoolean(getFields().size(), (Boolean) id);
        }else if (idType.isAssignableFrom(Double.class)) {
            preparedStatement.setDouble(getFields().size(), (Double) id);
        }
        int row = preparedStatement.executeUpdate();
    }
}
