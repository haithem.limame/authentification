package com.esgi.orm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private final static String DB_URL = "jdbc:mysql://localhost:3306/authentification";
    private final static String DRIVER_NAME = "com.mysql.jdbc.Driver";
    private final static String USER_NAME = "root";
    private final static String PASSWORD = "RootRoot";
    private static Connection con;

    private ConnectionManager() {

    }

    public static Connection getConnection() {
        try {
            Class.forName(DRIVER_NAME);
            try {
                con = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
            } catch (SQLException ex) {
                // log an exception. fro example:
                System.err.format("SQL State: %s\n%s", ex.getSQLState(), ex.getMessage());
            }
        } catch (ClassNotFoundException ex) {
            // log an exception. for example:
            ex.printStackTrace();
        }
        return con;
    }
}
