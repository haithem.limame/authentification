package com.esgi.service;

import com.esgi.model.User;
import com.esgi.repository.UserRepository;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class UserService {
    UserRepository repository = new UserRepository();

    public void createUser(User User) throws SQLException, IntrospectionException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        repository.save(User);
    }

    public void deleteUser(User User) throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        repository.deleteById(User.getId());
    }

    public User getUser(Integer id) throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return repository.findById(id).get();
    }

    public List<User> getUsers() throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return (List<User>) repository.findAll();
    }

    public Boolean isAuthentificate(String login,String password) throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Optional<User> user = getUsers().stream().filter(u-> u.getEmail().equals(login)&& u.getPassword().equals(password)).findFirst();
        return user.isPresent();
    }

    public void updatePassword(User user,String password) throws SQLException, IntrospectionException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        user.setPassword(password);
        repository.update(user);
    }
}
