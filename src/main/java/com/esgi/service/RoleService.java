package com.esgi.service;

import com.esgi.model.Role;
import com.esgi.repository.RoleRepository;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class RoleService {
    RoleRepository repository = new RoleRepository();

    public void createRole(Role role) throws SQLException, IntrospectionException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        repository.save(role);
    }

    public void deleteRole(Role role) throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        repository.deleteById(role.getId());
    }

    public Role getRole(Integer id) throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return repository.findById(id).get();
    }

    public List<Role> getRoles() throws SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return (List<Role>) repository.findAll();
    }
}
