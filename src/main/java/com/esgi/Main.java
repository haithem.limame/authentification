package com.esgi;

import com.esgi.repository.UserRepository;

public class Main {

    public static void main(String... args) throws Exception {
        UserRepository rep = new UserRepository();
        rep.findById(1);

        rep.findAll().forEach(System.out::println);
    }
}